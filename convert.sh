#!/bin/bash

for i in $(seq -f "%02g" 1 69)
do
  ffmpeg -pattern_type glob -framerate 100 -i "pages/g${i}-16[0123456789]*.png" -pix_fmt yuv420p -r 4 g$i.mp4
done

