#!/bin/bash
set -e

case "$1" in
    "server")
        uwsgi --ini uwsgi.ini
        ;;
    "capture")
        ./take_screenshots.sh
        ;;
esac
