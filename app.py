from flask import Flask, render_template, json

app = Flask(__name__)

with open('config.json', encoding='utf8') as file:
    config = json.load(file)


@app.route('/')
def index():
    return render_template('index.html', config=config)


if __name__ == '__main__':
    app.run()
