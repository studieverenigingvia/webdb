import * as fs from 'fs';
import puppeteer from "puppeteer";
import sharp from "sharp";

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

const run = async () => {
  console.log('Running screenshot capture sequentially...');

  let outdir = 'static/pages/';

  let sites = JSON.parse(fs.readFileSync('config.json', 'utf8')).sites;
  shuffleArray(sites);

  let now = new Date();

  let options = {
    executablePath: 'google-chrome-stable',
    args: ['--no-sandbox']
  };

  let width = 1920;
  let height = 1080;
  let thumbWidth = 1920 / 4;
  let thumbHeight = 1080 / 4;

  let timeToSleepForScreenshot = 10 * 1000;
  let navigationTimeout = 30 * 1000;

  const browser = await puppeteer.launch(options);
  for (let i = 0; i < sites.length; i++) {
    const site = sites[i];

    const page = await browser.newPage();
    try {
      // page.setJavaScriptEnabled(false);
      await page.setViewport({
        width: width,
        height: height
      });
      page.setDefaultNavigationTimeout(navigationTimeout);

      console.log('Capturing', site.name, site.url);
      await page.goto(site.url, {
        // consider navigation to be finished when there are no more than 2 network connections for at least 500 ms.
        'waitUntil': 'networkidle2'
      })

      console.log('Navigated and networkidle2, sleeping before screenshotting');
      await sleep(timeToSleepForScreenshot)

      console.log('Taking screenshot');

      let basename = outdir + site.name + '-';
      let latestpath = basename + 'latest.jpg';
      let latesthighrespath = basename + 'latest.png';
      let epochpath = basename + now.getTime() + '.png';

      await page.screenshot({
        path: epochpath
      });

      await sharp(epochpath)
        .resize(thumbWidth, thumbHeight)
        .toFile(latestpath, (err, info) => {
          if (err) {
            console.error('Error thumbnailing ' + site.name, err);
          }
        });

      fs.createReadStream(epochpath).pipe(fs.createWriteStream(latesthighrespath));
    } catch (e) {
      console.error('Error during navigating and screenshotting ' + site.name, e);
    } finally {
      await page.close();
    }
  }

  await browser.close();
}

await run();
